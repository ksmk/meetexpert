import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {DateAdapter} from '@angular/material';

@Component({
  selector: 'datetime-picker',
  templateUrl: './datetime-picker.component.html',
  styleUrls: ['./datetime-picker.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DatetimePickerComponent implements OnInit, OnChanges, OnDestroy {
  @Input() public minDate: Date;
  @Input() public maxDate: Date;
  @Input() public required = false;
  @Input() public labels = {
    date: 'label.date',
    hour: 'label.hour',
    minute: 'label.minute'
  };
  @Input() public defaultDate: Date = new Date();
  @Output() public ngModelChange: EventEmitter<Date> = new EventEmitter();

  public hoursArray: number[] = Array.from(new Array(24), (val, index) => index);
  public minutesArray: number[] = Array.from(new Array(60), (val, index) => index);
  public dateForm: FormGroup;
  private subscriber: Subscription[] = [];
  public timeEnabled = false;

  public on = {
    hoursChanged: (ev: any) => {
      const dateField = this.dateForm.get('date');
      if (dateField.value) {
        const date = dateField.value._isAMomentObject ? dateField.value.toDate() : dateField.value;
        dateField.setValue(
          new Date(date.setHours(ev.value))
        );
      }
    },
    minutesChanged: (ev: any) => {
      const dateField = this.dateForm.get('date');
      if (dateField.value) {
        const date = dateField.value._isAMomentObject ? dateField.value.toDate() : dateField.value;
        dateField.setValue(
          new Date(date.setMinutes(ev.value))
        );
      }
    }
  };

  constructor(
    private fb: FormBuilder,
    private dateAdapter: DateAdapter<Date>,
  ) {
    dateAdapter.setLocale('pl');
    this.createForm();
  }

  private createForm() {
    this.dateForm = this.fb.group({
      date: [null, this.required ? Validators.required : null],
      hours: [null, this.required ? Validators.required : null],
      minutes: [null, this.required ? Validators.required : null]
    });
  }

  ngOnInit() {
    const hoursField = this.dateForm.get('hours');
    const minutesField = this.dateForm.get('minutes');
    this.dateForm.get('date').valueChanges.subscribe(res => {
      console.warn(res);
      if (res instanceof Date) {
        res.setHours(hoursField.value);
        res.setMinutes(minutesField.value);
          this.ngModelChange.emit(res);
      } else {
          res.hour(hoursField.value);
          res.minute(minutesField.value);
          this.ngModelChange.emit(res.toDate());
      }
    });
  }

  ngOnChanges(changes) {
    if (changes.defaultDate) {
      const datetime = this.defaultDate;
      this.dateForm.patchValue({
        date: datetime,
        hours: datetime.getHours(),
        minutes: datetime.getMinutes()
      });
    }
  }

  ngOnDestroy() {
    this.subscriber.forEach(s => {
      s.unsubscribe();
    });
  }
}
